<?php
/**
*
* Caramel Lottery
*
* @copyright (c) 2015 Forum Promotion
* @license GNU General Public License, version 2 (GPL-2.0)
*
*/

namespace forumpromotion\lottery\core;

/**
* General, repeatable functions for caramel.
*/
class lottery_manager
{
	/** @var \phpbb\auth\auth */
	protected $auth;
	/** @var \phpbb\config\config */
	protected $config;
	/** @var \phpbb\db\driver\driver_interface */
	protected $db;
	/** @var \phpbb\notification\manager */
	protected $notification_manager;
	/** @var \phpbb\user */
	protected $user;

	/** @var string */
	protected $lottery_games_table;
	/** @var string */
	protected $lottery_tickets_table;

	/** @var string */
	protected $root_path;
	/** @var string */
	protected $php_ext;

	/**
	* Constructor 
	*/
	public function __construct(
		\phpbb\auth\auth $auth, 
		\phpbb\config\config $config, 
		\phpbb\db\driver\driver_interface $db, 
		\phpbb\notification\manager $notification_manager,
		\phpbb\user $user, 
		$lottery_games_table, 
		$lottery_tickets_table, 
		$root_path, 
		$php_ext
	)
	{
		$this->auth = $auth;
		$this->config = $config;
		$this->db = $db;
		$this->notification_manager = $notification_manager;
		$this->user = $user;

		$this->lottery_games_table = $lottery_games_table;
		$this->lottery_tickets_table = $lottery_tickets_table;

		$this->root_path = $root_path;
		$this->php_ext = $php_ext;
	}

    /**
     * Gets a list of games.
     *
     * @param int $start_item (optional) Starting point from pagination.
     * @param int $items_per_page (optional) The number of items to show. 0 = no limit.
     * @param bool $active_only (optional) Only show active items. Default true.
     * @param bool $future_only (optional) Only show future games. Default false.
     * @param bool $exclude_recurring (optional) Exclude recurring games. Default false.
     * @return array Array containing list of games as they are returned from the database.
     */
	public function get_games_list(
        $start_item = 0, $items_per_page = 0, $active_only = true, $future_only = false,
        $exclude_recurring = false
    )
	{
		if($this->is_lottery_enabled())
		{
			$sql_ary = array(
				'SELECT' => 'g.*',
				'FROM' => array($this->lottery_games_table => 'g'),
				'ORDER_BY' => 'game_end_date DESC',
                'WHERE' => '1=1',
			);

			if($active_only)
			{
				$sql_ary['WHERE'] .= ' AND game_active = 1';
			}
			if($future_only)
			{
				$sql_ary['WHERE'] .= ' AND game_end_date > ' . time();
			}
            if($exclude_recurring)
            {
                $sql_ary['WHERE'] .= ' AND game_recurring = 0';
            }

			$sql = $this->db->sql_build_query('SELECT', $sql_ary);

			if($items_per_page > 0)
			{
				$result = $this->db->sql_query_limit($sql, $items_per_page, $start_item);
			}
			else
			{
				$result = $this->db->sql_query($sql);
			}

			$games = $this->db->sql_fetchrowset($result);
			$this->db->sql_freeresult($result);

			return $games;
		}

		return array();
	}

	/**
	* Gets the row for an individual game.
	*
	* @param int  $game_id  The database key for the current game.
	* @return array   Data about the current game, as well as winner information.
	*/
	public function get_game($game_id)
	{
		$row = NULL;

		if($this->is_lottery_enabled())
		{
			$sql_ary = array(
				'SELECT' => 'g.*, u.user_id AS winner_id, u.username AS winner_username, u.user_colour AS winner_colour',
				'FROM' => array($this->lottery_games_table => 'g'),
				'LEFT_JOIN' => array(
					array(
						'FROM' => array(USERS_TABLE => 'u'),
						'ON' => 'u.user_id = g.game_winner'
					),
				),
				'WHERE' => 'game_id = ' . (int) $game_id,
			);
			$sql = $this->db->sql_build_query('SELECT', $sql_ary);
			$result = $this->db->sql_query($sql);
			$row = $this->db->sql_fetchrow($result);
		}

		return $row;
	}

    /**
     * Creates a game.
     *
     * @param $name
     * @param int $start_date (optional) UNIX time of start date. Defaults to current time.
     * @param int $end_date (optional) End date. Will use default length if none provided.
     * @param float $game_tax (optional) Tax in precise decimal from 0 to 1. Will use
     * @param float $ticket_cost (optional) Ticket cost. Will use default if none given.
     * @param int $max_tickets (optional) The max number of tickets that can be bought this round. 0 for unlimited.
     * @param int $max_tickets_user (optional) The max number of tickets a user can buy for this game. 0 for unlimted.
     * @param bool $recurring Is the game being edited recurring.
     * @return bool Success status.
     */
	public function create_game($name, $start_date = NULL, $end_date = NULL, $game_tax = NULL, $ticket_cost = NULL, $max_tickets = NULL, $max_tickets_user = NULL, $recurring = FALSE)
	{
		if($this->is_lottery_enabled())
		{
			$today_beginning = strtotime("midnight", time());
			$tomorrow_beginning = strtotime("tomorrow", $today_beginning) - 1;

			if(empty($start_date))
			{
				$start_date = $today_beginning;
			}

			if($start_date < $today_beginning)
			{
				return false;
			}

			if($game_tax < 0 || $game_tax > 1) 
			{
				$game_tax = 0; // Zero out invalid tax.
			}

			$game_active = ($start_date <= $today_beginning && ($end_date > $tomorrow_beginning || $end_date == 0));
			if($end_date == NULL || $end_date <= $start_date)
			{
				$seconds_to_add = $this->config['lottery_game_length'] * (24 * 60 * 60);
				$end_date = $start_date + $seconds_to_add;
			}

			$ticket_cost = ($ticket_cost == NULL) ? $this->config['lottery_ticket_cost'] : $ticket_cost;

			$max_tickets = (!isset($max_tickets)) ? $this->config['lottery_ticket_round_max'] : $max_tickets;
			$max_tickets_user = (!isset($max_tickets_user)) ? $this->config['lottery_ticket_user_max'] : $max_tickets_user;

			$data = array(
				'game_name'               => (string) $name,
				'game_active'             => (int) $game_active,
				'game_tax'                => (float) $game_tax,
				'game_start_date'         => (int) $start_date,
				'game_end_date'           => (int) $end_date,
				'game_ticket_cost'        => (float) $ticket_cost,
				'game_max_tickets'        => (int) $max_tickets,
				'game_max_tickets_user'   => (int) $max_tickets_user,
				'game_recurring'          => (int) ($recurring) ? 1 : 0,
			);

			$sql = 'INSERT INTO ' . $this->lottery_games_table . ' ' . $this->db->sql_build_array('INSERT', $data);
			$this->db->sql_query($sql);

            return true;
		}
	}

	/**
	* Updates the recurring games in the lottery.
	*
	* @return int  The number of rows affected (should be 0 or 1).
	*/
	public function update_recurring_game()
	{
		$affected_rows = 0;

		if($this->is_lottery_enabled())
		{
			$data = array(
				'game_name' => $this->config['lottery_recurring_name'],
				'game_ticket_cost' => $this->config['lottery_recurring_ticket_cost'],
				'game_max_tickets_user' => $this->config['lottery_recurring_max_tickets_user'],
				'game_max_tickets' => $this->config['lottery_recurring_max_tickets'],
			);

			$sql = 'UPDATE ' . $this->lottery_games_table . ' SET ' . $this->db->sql_build_array('UPDATE', $data) . ' WHERE game_recurring = 1 AND game_active = 1 AND game_end_date > ' . time();
			$this->db->sql_query($sql);

			$affected_rows = $this->db->sql_affectedrows();

            if(!$this->config['lottery_recurring_enabled'])
            {
                // Delete active.
                $sql = 'DELETE FROM ' . $this->lottery_games_table . ' WHERE game_recurring = 1 AND game_active = 1 AND game_end_date > ' . time();
                $this->db->sql_query($sql);
            }
		}

		return $affected_rows;
	}

	public function edit_game($game_id, $name, $start_date = NULL, $end_date = NULL, $game_tax = NULL, $ticket_cost = NULL, $max_tickets = NULL, $max_tickets_user = NULL, $recurring = FALSE)
	{
        if($this->is_lottery_enabled())
        {
            // TODO: Remove the stuff that is already being checked for in other functions.
            $today_beginning = strtotime("midnight", time());
            $tomorrow_beginning = strtotime("tomorrow", $today_beginning) - 1;

            if(empty($start_date))
            {
                $start_date = $today_beginning;
            }

            if($start_date < $today_beginning)
            {
                return false;
            }

            if($game_tax < 0 || $game_tax > 1)
            {
                $game_tax = 0; // Zero out invalid tax.
            }

            $game_active = ($start_date <= $today_beginning && ($end_date > $tomorrow_beginning || $end_date == 0));
            if($end_date == NULL || $end_date <= $start_date)
            {
                $seconds_to_add = $this->config['lottery_game_length'] * (24 * 60 * 60);
                $end_date = $start_date + $seconds_to_add;
            }

            $ticket_cost = ($ticket_cost == NULL) ? $this->config['lottery_ticket_cost'] : $ticket_cost;

            $max_tickets = (!isset($max_tickets)) ? $this->config['lottery_ticket_round_max'] : $max_tickets;
            $max_tickets_user = (!isset($max_tickets_user)) ? $this->config['lottery_ticket_user_max'] : $max_tickets_user;

            $data = array(
                'game_name'               => (string) $name,
                'game_active'             => (int) $game_active,
                'game_tax'                => (float) $game_tax,
                'game_start_date'         => (int) $start_date,
                'game_end_date'           => (int) $end_date,
                'game_ticket_cost'        => (float) $ticket_cost,
                'game_max_tickets'        => (int) $max_tickets,
                'game_max_tickets_user'   => (int) $max_tickets_user,
                'game_recurring'          => (int) ($recurring) ? 1 : 0,
            );

            $sql = 'UPDATE ' . $this->lottery_games_table . ' SET ' . $this->db->sql_build_array('UPDATE', $data);
            $this->db->sql_query($sql);

            return true;
        }
	}

	public function get_active_game_count($recurring_only = TRUE)
	{
		$sql_ary = array(
			'SELECT' => 'COUNT(game_id) as game_count',
			'FROM' => array($this->lottery_games_table => 'g'),
			'WHERE' => 'game_active = 1'
		);

		if($recurring_only)
		{
			$sql_ary['WHERE'] .= ' AND game_recurring = 1';
		}

		$sql = $this->db->sql_build_query('SELECT', $sql_ary);
		$result = $this->db->sql_query($sql);
		$count = $this->db->sql_fetchfield('game_count');

		return $count;
	}

	/**
	* Ends game that are still marked as active but have passed the end date.
	*/
	public function end_expired_games()
	{
		if($this->is_lottery_enabled())
		{
			$sql_ary = array(
				'SELECT' => 'game_id, game_jackpot, game_recurring',
				'FROM' => array($this->lottery_games_table => 'g'),
				'WHERE' => 'game_active = 1 AND game_end_date <= ' . time()
			);
			$sql = $this->db->sql_build_query('SELECT', $sql_ary);
			$result = $this->db->sql_query($sql);
			$rowset = $this->db->sql_fetchrowset($result);
			$this->db->sql_freeresult($result);

			foreach($rowset as $key => $value)
			{
				$this->end_game($value['game_id']);

				if($value['game_recurring'] == 1)
				{
					$data = array(
						'game_name' => $this->config['lottery_recurring_name'],
						'game_ticket_cost' => $this->config['lottery_recurring_ticket_cost'],
						'game_max_tickets' => $this->config['lottery_recurring_max_tickets'],
						'game_max_tickets_user' => $this->config['lottery_recurring_max_tickets_user']
					);

					$start_date = strtotime("midnight", time());
					$end_date = $start_date + ($this->config['lottery_recurring_length'] * 60 * 60 * 24) - 1;

					$this->create_game($data['game_name'], $start_date, $end_date, 0, $data['game_ticket_cost'], $data['game_max_tickets'], $data['game_max_tickets_user'], TRUE);
				}
			}
		}
	}

	/**
	* Ends the specified game.
	*
	* @param int   $game_id         The game to end.
	* @param bool  $choose_winner   (optional) Whether to choose a winner for the game.
	* @param bool  $refund          (optional) If no winner chosen, whether to supply refund.
	*/ 
	public function end_game($game_id, $choose_winner = TRUE, $refund = FALSE)
	{
		if($this->is_lottery_enabled())
		{
			$winner = 0;
			$game_id = (int) $game_id;

			if($choose_winner)
			{
				$winner = $this->choose_winner($game_id);
			}
			else if($refund)
			{
				// TODO: Give refund.
			}

			$data = array(
				'game_active' => 0,
				'game_winner' => $winner,
				'game_actual_end_date' => time(),
			);

			$sql = 'UPDATE ' . $this->lottery_games_table . '
				SET ' . $this->db->sql_build_array('UPDATE', $data) . '
				WHERE game_active = 1 AND game_id = ' . $game_id;
			$this->db->sql_query($sql);

			if($choose_winner)
			{
				$sql_ary = array(
					'SELECT' => 'game_name, game_jackpot',
					'FROM' => array($this->lottery_games_table => 'g'),
					'WHERE' => 'game_id = ' . (int) $game_id,
				);
				$sql = $this->db->sql_build_query('SELECT', $sql_ary);
				$result = $this->db->sql_query($sql);
				$row = $this->db->sql_fetchrow($result);

				$jackpot = $row['game_jackpot'];
				$game_title = $row['game_name'];

                if($winner != ANONYMOUS && $winner > 0)
                {
                    $sql = 'UPDATE ' . USERS_TABLE . '
                        SET user_crml_cash = user_crml_cash + ' . (int) $jackpot . '
                        WHERE user_id = ' . (int) $winner;
                    $this->db->sql_query($sql);
                }

				$this->notification_manager->add_notifications(
					array('forumpromotion.lottery.notification.type.win'), 
					array(
						'user_id_to'   => (int) $winner,
						'cash_amount'  => $jackpot,
						'game_id'      => (int) $game_id,
						'game_title'   => $game_title
					)
				);
			}
		}
	}

	/**
	* Buys a specified number of tickets for the current user for the specified game.
	* 
	* @param int   $game_id        The id of the current game.
	* @param int   $ticket_amount  The number of tickets to buy.
	* @return bool  The success status of the operation.
	*/
	public function buy_ticket($game_id, $ticket_amount, $game_jackpot_incr, $cost)
	{
		if($this->is_lottery_enabled())
		{
			$uid = (int) $this->user->data['user_id'];

			if($ticket_amount < 0)
			{
				return FALSE;
			}

			$sql_ary = array(
				'SELECT' => 'ticket_id',
				'FROM' => array($this->lottery_tickets_table => 't'),
				'WHERE' => 'ticket_user_id = ' . $uid . ' AND ticket_game_id = ' . (int) $game_id,
			);
			$sql = $this->db->sql_build_query('SELECT', $sql_ary);
			$result = $this->db->sql_query($sql);
			$ticket_row = $this->db->sql_fetchrow($result);
			$this->db->sql_freeresult($result);

			$sql = '';
			if($ticket_row)
			{
				// Already exists.
				$sql = 'UPDATE ' . $this->lottery_tickets_table . ' 
					SET ticket_count = ticket_count + ' . (int) $ticket_amount . ',
						ticket_purchase_time = ' . time() . '
					WHERE ticket_user_id = ' . $uid . ' AND ticket_game_id = ' . (int) $game_id;
			}
			else
			{
				$data = array(
					'ticket_count' => $ticket_amount,
					'ticket_user_id' => $this->user->data['user_id'],
					'ticket_game_id' => $game_id,
					'ticket_purchase_time' => time()
				);
				// Create one.
				$sql = 'INSERT INTO ' . $this->lottery_tickets_table . ' ' . $this->db->sql_build_array('INSERT', $data);
			}

			$this->db->sql_query($sql);

			// Update lottery table with tickets bought and jackpot.
			$sql = 'UPDATE ' . $this->lottery_games_table . ' SET game_tickets_bought = game_tickets_bought + ' . (int) $ticket_amount . ', game_jackpot = game_jackpot + ' . $game_jackpot_incr . ' WHERE game_id = ' . (int) $game_id;
			$this->db->sql_query($sql);

			// Deduct cash from user.
			$sql = 'UPDATE ' . USERS_TABLE . ' 
				SET user_crml_cash = user_crml_cash - ' . $cost . ' 
				WHERE user_id = ' . $this->user->data['user_id'];
			$this->db->sql_query($sql);

			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}

	/** 
	* Chooses a winner for the selected game. Does not end up setting the winner for the game, but simply returns the chosen winner.
	* @param int   $game_id  The game to choose a winner for.
	* @return int  The id of the selected user.
	*/
	public function choose_winner($game_id)
	{
		$game_id = (int) $game_id;
		$winning_user = 0;

		// Get tickets.
		$sql_ary = array(
			'SELECT' => 'ticket_id, ticket_count, ticket_user_id',
			'FROM'   => array($this->lottery_tickets_table => 't'),
			'WHERE'  => 'ticket_game_id = ' . $game_id,
		);
		$sql = $this->db->sql_build_query('SELECT', $sql_ary);
		$result = $this->db->sql_query($sql);
		$ticket_rows = $this->db->sql_fetchrowset($result);
		$this->db->sql_freeresult($result);

		// Get total tickets.
		$sql_ary['SELECT'] = 'SUM(ticket_count) AS total_tickets';
		$sql = $this->db->sql_build_query('SELECT', $sql_ary);
		$result = $this->db->sql_query($sql);
		$total_tickets = (int) $this->db->sql_fetchfield('total_tickets');
		$this->db->sql_freeresult($result);

		if($total_tickets > 0)
		{
			$winning_ticket = mt_rand(1, $total_tickets);
			$user_ranges = array();
			$range_start = 0;
			$range_end = 0;

			foreach($ticket_rows as $ticket_row_key => $ticket_row)
			{
				$range_start = $range_end + 1;
				$range_end = $range_start + $ticket_row['ticket_count'];

				// If user is within range, they won! Yay for winning!
				if($range_start <= $winning_ticket && $range_end >= $winning_ticket)
				{
					$winning_user = $ticket_row['ticket_user_id'];
					break;
				}
			}
		}

		return $winning_user;
	}

	/**
	* Returns whether or not the lottery is enabled. Considers Caramel install status.
	*
	* @return bool   Whether or not the lottery is enabled.
	*/
	public function is_lottery_enabled()
	{
		return (
			($this->config['lottery_enabled'] == 1) 
			&& isset($this->config['crml_enabled'])
			&& $this->config['crml_enabled'] == 1
		);
	}
}