<?php
/**
*
* Caramel Lottery
*
* @copyright (c) 2015 Forum Promotion
* @license GNU General Public License, version 2 (GPL-2.0)
*
*/

namespace forumpromotion\lottery\migrations\v1;

class m1_schema_foundations extends \phpbb\db\migration\migration
{
	public function update_schema()
	{
		return array(
			'add_tables' => array(
				$this->table_prefix . 'lottery_games' => array(
					'COLUMNS' => array(
						'game_id'                  => array('UINT', NULL, 'auto_increment'),
						'game_active'              => array('BOOL', 0),
						'game_name'                => array('VCHAR_UNI:255', ''),
						'game_tax'                 => array('PDECIMAL', 0),
						'game_tickets_bought'      => array('UINT', 0),
						'game_start_date'          => array('TIMESTAMP', 0),
						'game_end_date'            => array('TIMESTAMP', 0),
						'game_actual_end_date'     => array('TIMESTAMP', 0),
						'game_winner'              => array('UINT', 0),
						'game_ticket_cost'         => array('DECIMAL', 0),
						'game_max_tickets'         => array('UINT', 0),
						'game_max_tickets_user'    => array('UINT', 0),
						'game_jackpot'             => array('UINT', 0),
						'game_recurring'           => array('BOOL', 0),
					),
					'PRIMARY_KEY' => 'game_id',
					'KEYS' => array(
						'gmact' => array('INDEX', 'game_active'),
						'strtdt' => array('INDEX', 'game_start_date'),
						'enddt' => array('INDEX', 'game_end_date'),
						'cost' => array('INDEX', 'game_ticket_cost'),
						'winner' => array('INDEX', 'game_winner'),
					),
				),
				$this->table_prefix . 'lottery_tickets' => array(
					'COLUMNS' => array(
						'ticket_id'              => array('UINT', NULL, 'auto_increment'),
						'ticket_count'           => array('UINT', 0),
						'ticket_user_id'         => array('UINT', 0),
						'ticket_game_id'         => array('UINT', 0),
						'ticket_purchase_time'   => array('TIMESTAMP', 0),
					),
					'PRIMARY_KEY' => 'ticket_id',
					'KEYS' => array(
						'uid' => array('INDEX', 'ticket_user_id'),
						'gid' => array('INDEX', 'ticket_game_id'),
					),
				),
			),
		);
	}

	public function revert_schema()
	{
		return array(
			'drop_tables' => array(
				$this->table_prefix . 'lottery_games',
				$this->table_prefix . 'lottery_tickets'
			)
		);
	}
}