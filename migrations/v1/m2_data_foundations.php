<?php
/**
*
* Caramel Lottery
*
* @copyright (c) 2015 Forum Promotion
* @license GNU General Public License, version 2 (GPL-2.0)
*
*/

namespace forumpromotion\lottery\migrations\v1;

class m2_data_foundations extends \phpbb\db\migration\migration
{
	public function update_data()
	{
		return array(
			array('permission.add', array('m_lottery_manage')),
			array('permission.add', array('u_lottery_play')),

			array('permission.permission_set', array('ROLE_ADMIN_STANDARD', 'm_lottery_manage')),
			array('permission.permission_set', array('ROLE_USER_STANDARD', array('u_lottery_play'))),
			array('permission.permission_set', array('ROLE_USER_FULL', array('u_lottery_play'))),

			array('config.add', array('lottery_enabled', 1)),
			array('config.add', array('lottery_name', 'Lottery')),
			array('config.add', array('lottery_ticket_cost', 10)),
			array('config.add', array('lottery_ticket_round_max', 1000)),
			array('config.add', array('lottery_ticket_user_max', 300)),
			array('config.add', array('lottery_game_length', 10)),
			array('config.add', array('lottery_tax', 0)),

			array('config.add', array('lottery_recurring_enabled', 0)),
			array('config.add', array('lottery_recurring_name', '')),
			array('config.add', array('lottery_recurring_length', 0)), // in days
			array('config.add', array('lottery_recurring_ticket_cost', 0)),
			array('config.add', array('lottery_recurring_max_tickets_user', 0)),
			array('config.add', array('lottery_recurring_max_tickets', 0)),

			array('module.add', array('ucp', '', 'UCP_LOTTERY')),
			array('module.add', array(
				'ucp',
				'UCP_LOTTERY',
				array(
					'module_basename' => '\forumpromotion\lottery\ucp\lottery_module',
					'modes' => array('select_game', 'play_game', 'past_winners'),
				)
			)),

			array('module.add', array('mcp', '', 'MCP_LOTTERY')),
			array('module.add', array(
				'mcp',
				'MCP_LOTTERY',
				array(
					'module_basename' => '\forumpromotion\lottery\mcp\manage_lottery_module',
					'modes' => array('manage_games', 'create_game', 'create_recurring_game')
				)
			)),
		);
	}
}