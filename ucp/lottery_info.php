<?php
/**
*
* Lottery for Caramel
*
* @copyright (c) 2015 Forum Promotion
* @license GNU General Public License, version 2 (GPL-2.0)
*
*/

namespace forumpromotion\lottery\ucp;

class lottery_info
{
	function module()
	{
		return array(
			'filename'   => '\forumpromotion\lottery\ucp\lottery_module',
			'title'      => 'UCP_LOTTERY',
			'modes'     => array(
				'select_game' => array(
					'title' => 'UCP_LOTTERY_SELECT_GAME',
					'auth'  => 'ext_forumpromotion/lottery && ext_forumpromotion/caramel',
					'cat'   => array('UCP_LOTTERY')
				),
				'play_game' => array(
					'title' => 'UCP_LOTTERY_PLAY_GAME',
					'auth'  => 'ext_forumpromotion/lottery && ext_forumpromotion/caramel',
					'cat'   => array('UCP_LOTTERY'),
					'display' => false
				),
				'past_winners' => array(
					'title' => 'UCP_LOTTERY_PAST_WINNERS',
					'auth'  => 'ext_forumpromotion/lottery && ext_forumpromotion/caramel',
					'cat'   => array('UCP_LOTTERY')
				),

			) 
		);
	}
}