<?php
/**
*
* Lottery for Caramel
*
* @copyright (c) 2015 Forum Promotion
* @license GNU General Public License, version 2 (GPL-2.0)
*
*/

namespace forumpromotion\lottery\ucp;

/**
* UCP Module for Lottery
*/
class lottery_module
{
	/** @var string */
	public $u_action;

	/** @var int */
	protected $id;
	/** @var int */
	protected $mode;

	/** @var \phpbb\config\config */
	protected $config;
	/** @var \phpbb\db\driver\driver_interface */
	protected $db;
	/** @var \phpbb\log\log */
	protected $phpbb_log;
	/** @var \phpbb\request\request */
	protected $request;
	/** @var \phpbb\symfony_request */
	protected $symfony_request;
	/** @var \phpbb\template\template */
	protected $template;
	/** @var \phpbb\user */
	protected $user;

	protected $p_master;
	protected $module;

	protected $lottery_manager;

	protected $lottery_enabled;

	protected $lottery_games_table;
	protected $lottery_tickets_table;

	/**
	* Constructor
	*/
	public function __construct($p_master)
	{
		global $db, $user, $auth, $template, $cache, $request, $symfony_request;
		global $config, $phpbb_root_path, $phpbb_admin_path, $phpEx, $pagination;
		global $phpbb_log, $table_prefix;
		global $phpbb_container;
		global $module;

		$this->p_master = $p_master;
		$this->module   = $module;

		$this->phpEx = $phpEx;
		$this->root_path = $phpbb_root_path;

		$this->config    = $config;
		$this->db        = $db;
		$this->user      = $user;
		$this->phpbb_log = $phpbb_log;
		$this->request   = $request;
		$this->symfony_request = $symfony_request;
		$this->template  = $template;

		$this->pagination = $phpbb_container->get('pagination');
		$this->lottery_manager = $phpbb_container->get('forumpromotion.lottery.core.manager');

		$this->lottery_enabled = FALSE;

		$this->lottery_games_table = $table_prefix . 'lottery_games';
		$this->lottery_tickets_table = $table_prefix . 'lottery_tickets';
	}

	/**
	* Entry point for module.
	*
	* @param int $id       The id of the module.
	* @param string $mode  The mode of the module.
	*/
	public function main($id, $mode)
	{
		$this->id = $id;
		$this->mode = $mode;

		$this->lottery_enabled = $this->lottery_manager->is_lottery_enabled();

		if($this->lottery_enabled)
		{
			$this->lottery_manager->end_expired_games();
			
			switch($this->mode)
			{
				case 'select_game':
					$this->select_game();
					break;
				case 'play_game':
					$this->play_game();
					break;
				case 'past_winners':
					$this->past_winners();
					break;
			}
		}
		else
		{
			trigger_error('LOTTERY_DISABLED');
		}
	}

	public function select_game()
	{
		// Gets list of active games.
		$games = $this->lottery_manager->get_games_list(0, 0, true, false, !$this->config['lottery_recurring_enabled']);

		foreach($games as $game_key => $game)
		{
			$play_url_params = array(
				'i=-forumpromotion-lottery-ucp-lottery_module',
				'mode=play_game',
				'id=' . $game['game_id']
			);

			$this->template->assign_block_vars('games', array(
				'NAME'             => $game['game_name'],
				'TAX_PERCENT'      => ((string)($game['game_tax'] * 100)) + "%",
				'TICKETS_BOUGHT'   => (int) $game['game_tickets_bought'],
				'START_DATE'       => $this->user->format_date($game['game_start_date']),
				'END_DATE'         => $this->user->format_date($game['game_end_date']),
                'RECURRING'        => $game['game_recurring'],
				'TICKET_COST'      => $game['game_ticket_cost'],
				'MAX_TICKETS'      => $game['game_max_tickets'],
				'MAX_TICKETS_USER' => $game['game_max_tickets_user'],
				'PLAY_URL'         => append_sid($this->root_path . 'ucp.' . $this->phpEx, implode('&amp;', $play_url_params))
			));
		}

		$this->template->assign_vars(array(
			'S_LOTTERY_GAMES_COUNT' => count($games)
		));

		$this->tpl_name = 'ucp_select_game';
		$this->page_title = $this->user->lang('UCP_LOTTERY_SELECT_GAME');
	}

	public function play_game()
	{
		$error = '';
		$game_id = $this->request->variable('id', 0);
		$current_game = $this->lottery_manager->get_game($game_id);
		$tickets_bought = 0;

		$this->module->set_display($this->id, 'play_game', true);

		if($current_game)
		{
			$sql_ary = array(
				'SELECT' => 'SUM(ticket_count) AS my_ticket_count',
				'FROM' => array($this->lottery_tickets_table => 't'),
				'WHERE' => 'ticket_user_id = ' . $this->user->data['user_id'] . ' AND ticket_game_id = ' . (int) $game_id,
			);
			$sql = $this->db->sql_build_query('SELECT', $sql_ary);
			$result = $this->db->sql_query($sql);
			$my_tickets_purchased = $this->db->sql_fetchfield('my_ticket_count', $result);
			$this->db->sql_freeresult($result);

			if($my_tickets_purchased == NULL)
			{
				$my_tickets_purchased = 0;
			}

			if($this->request->is_set_post('submit'))
			{
				$tickets_bought = $this->request->variable('tickets_bought', 0);
				$game_jackpot_incr = $current_game['game_ticket_cost'] * $tickets_bought;
				$cost = $game_jackpot_incr;

				if((($my_tickets_purchased + $tickets_bought) > $current_game['game_max_tickets_user'])
					&& $current_game['game_max_tickets_user'] != 0)
				{
					$error = $this->user->lang('LOT_TICKET_MAX_USER_EXCEEDED');
				}

				if((($current_game['game_tickets_bought'] + $tickets_bought) > $current_game['game_max_tickets']) 
					&& $current_game['game_max_tickets'] != 0)
				{
					$error = $this->user->lang('LOT_TICKET_MAX_EXCEEDED');
				}

				if($tickets_bought <= 0)
				{
					$error = $this->user->lang('LOT_TICKET_GT_ZERO');
				}

				$sql_ary = array(
					'SELECT' => 'user_crml_cash',
					'FROM' => array(USERS_TABLE => 'u'),
					'WHERE' => 'user_id = ' . $this->user->data['user_id'],
				);
				$sql = $this->db->sql_build_query('SELECT', $sql_ary);
				$result = $this->db->sql_query($sql);
				$my_cash = $this->db->sql_fetchfield('user_crml_cash');
				$this->db->sql_freeresult($result);

				if($cost > $my_cash)
				{
					$error = $this->user->lang('LOT_INSUFFICIENT_CASH');
				}

				if(empty($error))
				{
					$this->lottery_manager->buy_ticket($game_id, $tickets_bought, $game_jackpot_incr, $cost);

					$play_url_params = array(
						'i=-forumpromotion-lottery-ucp-lottery_module',
						'mode=play_game',
						'id=' . $current_game['game_id']
					);
					$return_url = append_sid($this->root_path . 'ucp.' . $this->phpEx, implode('&amp;', $play_url_params));

					trigger_error(
						$this->user->lang('LOT_BUY_SUCCEED') 
						. '<br /><br /><a href="' . $return_url . '">' 
						. $this->user->lang('LOT_BUY_LINK_RETURN')
						. '</a>'
					);
				}
			}

			$winner = $current_game['game_active'] ? '' : get_username_string('full', $current_game['winner_id'],
                $current_game['winner_username'], $current_game['winner_colour'], $this->user->lang('LOTTERY_NONE'));

			$this->template->assign_vars(array(
				'S_ERROR' => !empty($error),
				'ERROR_MSG' => $error,

				'LOT_NAME' => $current_game['game_name'],
				'LOT_JACKPOT' => $current_game['game_jackpot'],
				'LOT_ACTIVE' => $current_game['game_active'],
				'LOT_TAX' => $current_game['game_tax'],
				'LOT_TICKETS_BOUGHT' => $current_game['game_tickets_bought'],
				'LOT_START_DATE' => $this->user->format_date($current_game['game_start_date']),
				'LOT_END_DATE' => $this->user->format_date($current_game['game_end_date']),
				'LOT_ACTUAL_END_DATE' => $this->user->format_date($current_game['game_actual_end_date']),
				'LOT_WINNER' => $winner,
				'LOT_TICKET_COST' => $current_game['game_ticket_cost'],
				'LOT_MAX_TICKETS' => $current_game['game_max_tickets'],
				'LOT_MAX_TICKETS_USER' => $current_game['game_max_tickets_user'],

				'S_LOT_TICKETS_BOUGHT' => 0,
				'S_LOT_MY_TICKETS' => $my_tickets_purchased
			));

			$this->tpl_name = 'ucp_play_game';
			$this->page_title = $current_game['game_name'];
		}
		else
		{
			trigger_error($this->user->lang('UCP_LOTTERY_GAME_NOT_EXIST'));
		}
	}

	public function past_winners()
	{
		$start_item = (int) $this->request->variable('start', 0);
		$items_per_page = 15;

		$sql_ary = array(
			'SELECT' => 'g.game_name, g.game_winner, g.game_actual_end_date, g.game_jackpot, u.username, u.user_colour',
			'FROM' => array($this->lottery_games_table => 'g'),
			'LEFT_JOIN' => array(
				array(
					'FROM' => array(USERS_TABLE => 'u'),
					'ON' => 'u.user_id = g.game_winner',
				),
			),
			'ORDER_BY' => 'g.game_actual_end_date DESC',
			'WHERE' => 'game_winner > 0'
		);

		$sql = $this->db->sql_build_query('SELECT', $sql_ary);
		$result = $this->db->sql_query_limit($sql, $items_per_page, $start_item);

		$games_count = 0;

		while($row = $this->db->sql_fetchrow($result))
		{
			$winner = get_username_string('full', $row['game_winner'], $row['username'],
                $row['user_colour'], $this->user->lang('LOTTERY_NONE'));

			$this->template->assign_block_vars('games', array(
				'NAME' => $row['game_name'],
				'WINNER' => $winner,
				'END_DATE' => $this->user->format_date($row['game_actual_end_date']),
				'JACKPOT' => $row['game_jackpot'],
			));

			$games_count++;
		}

		$sql_ary['SELECT'] = 'COUNT(game_id) AS total_games';
		$sql_ary['LEFT_JOIN'] = NULL;
		$sql = $this->db->sql_build_query('SELECT', $sql_ary);
		$result = $this->db->sql_query($sql);
		$total_games = (int) $this->db->sql_fetchfield('total_games');

		$params = array('i=-forumpromotion-lottery-ucp-lottery_module', 'mode=past_winners');
		$pagination_url = append_sid($this->root_path . 'ucp.' . $this->phpEx, implode('&amp;', $params));

		$pagination = $this->pagination->generate_template_pagination(
			$pagination_url, 
			'pagination', 
			'start', 
			$total_games, 
			$items_per_page, 
			$start_item
		);

		$this->template->assign_vars(array(
			'S_LOTTERY_GAMES_COUNT' => $games_count,
			'TOTAL_GAMES'           => $total_games,
			'PAGE_NUMBER'           => $this->pagination->on_page($total_games, $items_per_page, $start_item),
		));

		$this->tpl_name = 'ucp_past_winners';
		$this->page_title = $this->user->lang('UCP_LOTTERY_PAST_WINNERS');
	}
}