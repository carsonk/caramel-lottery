<?php
/**
*
* Lottery for Caramel
*
* @copyright (c) 2015 Forum Promotion
* @license GNU General Public License, version 2 (GPL-2.0)
*
*/

if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

$lang = array_merge($lang, array(
	'UCP_LOTTERY' => 'Lottery',

	'UCP_LOTTERY_SELECT_GAME' => 'Select game',
	'UCP_LOTTERY_PLAY_GAME' => 'Play game',
	'UCP_LOTTERY_PAST_WINNERS' => 'View past winners',

	'LOTTERY_NO_GAMES' => 'There are no active lottery games.',
	'LOTTERY_NO_COMPLETED_GAMES' => 'There are no completed games.',

	'LOTTERY_NAME' => 'Name',
	'LOTTERY_END_TIME' => 'End Time',
	'LOTTERY_TICKET_COST' => 'Ticket Cost',
    'LOTTERY_RECURRING' => 'Recurring',

	'LOTTERY_DISABLED' => 'Either Caramel or the Lottery is disabled.',

	'UCP_LOTTERY_GAME_NOT_EXIST' => 'That game does not exist.',

	'LOT_TICKETS_BOUGHT' => 'Total tickets bought',
	'LOT_TIME_RANGE' => 'Time range',
	'LOT_JACKPOT' => 'Jackpot',
	'LOT_MY_TICKETS' => 'Tickets I bought',
	'LOT_MAX_TICKETS' => 'Max tickets this game',
	'LOT_MAX_TICKETS_USER' => 'Max tickets per user',
	'LOT_PURCHASE_TICKETS' => 'Purchase tickets',
	'LOT_TICKET_COST' => 'Ticket cost',
	'LOT_TICKETS_TO_BUY' => 'Tickets to buy',
	'LOT_PURCHASE' => 'Purchase',
	'LOT_WINNER' => 'Winner',

	'LOT_TICKET_MAX_USER_EXCEEDED' => 'The amount you requested to brought your ticket count over the allowed amount for this game.',
	'LOT_TICKET_MAX_EXCEEDED' => 'The amount of tickets you requested would exceed the max tickets for this game.',
	'LOT_INSUFFICIENT_CASH' => 'You do not have enough cash for that number of tickets.',
	'LOT_TICKET_GT_ZERO' => 'Tickets purchased must be greater than zero.',

	'LOT_BUY_SUCCEED' => 'Tickets bought successfuly.',
	'LOT_BUY_LINK_RETURN' => 'Return to Lottery page'
));
