<?php
/**
*
* Lottery for Caramel
*
* @copyright (c) 2015 Forum Promotion
* @license GNU General Public License, version 2 (GPL-2.0)
*
*/

if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

$lang = array_merge($lang, array(
	'LOTTERY' => 'Lottery',

    'LOTTERY_NONE' => 'None',

	'NOTIFICATION_WIN' => 'You won %d %s in a lottery game!',
	'NOTIFICATION_WIN_OPTION' => 'You win a lottery game.'
));