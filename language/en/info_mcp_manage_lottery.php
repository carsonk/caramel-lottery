<?php
/**
*
* Lottery for Caramel
*
* @copyright (c) 2015 Forum Promotion
* @license GNU General Public License, version 2 (GPL-2.0)
*
*/

if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

$lang = array_merge($lang, array(
	'MCP_LOTTERY' => 'Lottery',
	'MCP_LOTTERY_MANAGE_GAMES' => 'Mange games',
	'MCP_LOTTERY_CREATE_GAME' => 'Create game',
    'MCP_LOTTERY_EDIT_GAME' => 'Edit game',
	'MCP_LOTTERY_CREATE_RECURRING_GAME' => 'Manage recurring game',

	'MCP_LOTTERY_MANAGE_EDIT' => 'Edit game',

	'MCP_LOTTERY_NAME' => 'Name',
	'MCP_START_TIME' => 'Start time',
	'MCP_END_TIME' => 'End time',
	'MCP_TICKET_COST' => 'Ticket cost',
	'MCP_MAX_TICKETS' => 'Max tickets',
	'MCP_MAX_TICKETS_USER' => 'Max tickets per user',

	'MCP_LOTTERY_INVALID_GAME' => 'There was no game selected, or the selected game does not exist.',

	'MCP_INVALID_START_DATE' => 'The start date was invalid.',
	'MCP_INVALID_END_DATE' => 'The end date was invalid.',
	'MCP_LOTTERY_START_DATE_TOO_LOW' => 'Start date cannot be in past.',
	'MCP_LOTTERY_END_DATE_TOO_LOW' => 'The end date should be set beyond the start date.',
	'MCP_INVALID_NAME_LENGTH' => 'The name of the game cannot be more than 50 characters, and must be at least two characters long.',
	'MCP_LOTTERY_INVALID_TICKET_COST' => 'Ticket cost must be a number greater than or equal to 0.',
	'MCP_LOTTERY_INVALID_MAX_TICKETS' => 'Field specifying max tickets must be greater than or equal to 0.',
	'MCP_LOTTERY_INVALID_MAX_TICKETS_USER' => 'Field specifying max tickets per user must be greater than or equal to 0.',
	'MCP_LOTTERY_INVALID_GAME_LENGTH' => 'The specified game length was invalid.',

	'MCP_LOTTERY_CREATE_SUCCEEDED' => 'Game was created successfully.',
    'MCP_LOTTERY_EDIT_SUCCEEDED' => 'Game was edited successfully.',
	'MCP_LOTTERY_RETURN' => 'Return to previous page',

	'MCP_GAME_LENGTH' => 'Game length (days)',
	'MCP_RECURRING_ENABLED' => 'Enable recurring game',
    'MCP_RECURRING_DEL_WARNING' => 'Disabling a recurring game will delete the active one.',

	'LOTTERY_NO_GAMES' => 'There are no active lottery games.',
	'LOTTERY_NAME' => 'Game name',
	'LOTTERY_END_TIME' => 'End time',
	'LOTTERY_TICKET_COST' => 'Ticket cost',
	'LOTTERY_TOOLS' => 'Lottery tools',
	'LOTTERY_EDIT' => 'Edit',
    'LOTTERY_PLAY' => 'Play',
    'MCP_LOT_WARNING' => 'Warning',
));
