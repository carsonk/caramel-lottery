<?php
/**
*
* Caramel Lottery
*
* @copyright (c) 2015 Forum Promotion
* @license GNU General Public License, version 2 (GPL-2.0)
*
*/

namespace forumpromotion\lottery\mcp;

class manage_lottery_info
{
	function module()
	{
		return array(
			'filename'   => '\forumpromotion\lottery\mcp\manage_lottery_module',
			'title'      => 'MCP_LOTTERY',
			'modes'     => array(
				'manage_games' => array(
					'title' => 'MCP_LOTTERY_MANAGE_GAMES',
					'auth' => 'ext_forumpromotion/lottery && acl_m_lottery_manage',
					'cat' => array('MCP_LOTTERY'),
				),
				'create_game' => array(
					'title' => 'MCP_LOTTERY_CREATE_GAME',
					'auth'  => 'ext_forumpromotion/lottery && acl_m_lottery_manage',
					'cat'   => array('MCP_LOTTERY'),
				),
				'create_recurring_game' => array(
					'title' => 'MCP_LOTTERY_CREATE_RECURRING_GAME',
					'auth' => 'ext_forumpromotion/lottery && acl_m_lottery_manage',
					'cat' => array('MCP_LOTTERY')
				),
			),
		);
	}
}