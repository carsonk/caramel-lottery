<?php
/**
*
* Lottery for Caramel
*
* @copyright (c) 2015 Forum Promotion
* @license GNU General Public License, version 2 (GPL-2.0)
*
*/

namespace forumpromotion\lottery\mcp;

/**
* MCP Module for Lottery
*/
class manage_lottery_module
{
    /** @var string */
    public $tpl_name;
    /** @var string */
    public $page_title;
	/** @var string */
	public $u_action;
	/** @var int */
	protected $id;
	/** @var int */
	protected $mode;

	/** @var \phpbb\config\config */
	protected $config;
	/** @var \phpbb\db\driver\driver_interface */
	protected $db;
	/** @var \phpbb\log\log */
	protected $phpbb_log;
	/** @var \phpbb\request\request */
	protected $request;
	/** @var \phpbb\symfony_request */
	protected $symfony_request;
	/** @var \phpbb\template\template */
	protected $template;
	/** @var \phpbb\user */
	protected $user;

	protected $p_master;
	protected $module;

    /** @var \forumpromotion\lottery\core\lottery_manager */
	protected $lottery_manager;

	protected $lottery_enabled;

    /**
     * Constructor
     * @param $p_master
     */
	public function __construct($p_master)
	{
		global $db, $user, $auth, $template, $cache, $request, $symfony_request;
		global $config, $phpbb_root_path, $phpbb_admin_path, $phpEx, $pagination;
		global $phpbb_log;
		global $phpbb_container;
		global $module;

		$this->p_master = $p_master;
		$this->module   = $module;

		$this->phpEx = $phpEx;
		$this->root_path = $phpbb_root_path;

		$this->config    = $config;
		$this->db        = $db;
		$this->user      = $user;
		$this->phpbb_log = $phpbb_log;
		$this->request   = $request;
		$this->symfony_request = $symfony_request;
		$this->template  = $template;

		$this->pagination = $phpbb_container->get('pagination');
		$this->lottery_manager = $phpbb_container->get('forumpromotion.lottery.core.manager');

		$this->lottery_enabled = FALSE;

	}

	/**
	* Entry point for module.
	*
	* @param int $id       The id of the module.
	* @param string $mode  The mode of the module.
	*/
	public function main($id, $mode)
	{
		$this->id = $id;
		$this->mode = $mode;

		$this->lottery_enabled = $this->lottery_manager->is_lottery_enabled();

		if($this->lottery_enabled)
		{
			$this->lottery_manager->end_expired_games();

			switch($this->mode)
			{
				case 'manage_games':
					$this->manage_games();
					break;
				case 'create_game':
					$this->create_game();
					break;
				case 'create_recurring_game':
					$this->create_recurring_game();
					break;
			}
		}
		else
		{
			trigger_error('LOTTERY_DISABLED');
		}
	}

	public function manage_games()
	{
		$games = $this->lottery_manager->get_games_list(0, 0, false, true, true);

		foreach($games as $game_key => $game)
		{
			$play_url_params = array(
				'i=-forumpromotion-lottery-ucp-lottery_module',
				'mode=play_game',
				'id=' . $game['game_id']
			);
            $play_url = append_sid($this->root_path . 'ucp.' . $this->phpEx, implode('&amp;', $play_url_params));

            $edit_url_params = array(
                'i=-forumpromotion-lottery-mcp-manage_lottery_module',
                'mode=create_game',
                'edit=' . $game['game_id']
            );
            $edit_url = append_sid($this->root_path . 'mcp.' . $this->phpEx, implode('&amp;', $edit_url_params));

			$this->template->assign_block_vars('games', array(
				'NAME'             => $game['game_name'],
				'TAX_PERCENT'      => ((string)($game['game_tax'] * 100)) + "%",
				'TICKETS_BOUGHT'   => (int) $game['game_tickets_bought'],
				'START_DATE'       => $this->user->format_date($game['game_start_date']),
				'END_DATE'         => $this->user->format_date($game['game_end_date']),
				'TICKET_COST'      => $game['game_ticket_cost'],
				'MAX_TICKETS'      => $game['game_max_tickets'],
				'MAX_TICKETS_USER' => $game['game_max_tickets_user'],

				'U_PLAY'         => $play_url,
                'U_EDIT'         => $edit_url,
			));
		}

		$this->template->assign_vars(array(
			'S_LOTTERY_GAMES_COUNT' => count($games)
		));

		$this->tpl_name = 'mcp_manage_games';
		$this->page_title = $this->user->lang('MCP_LOTTERY_MANAGE_GAMES');
	}

	public function create_game()
	{
		$errors = array();
		$submit_posted = $this->request->is_set_post('submit');
        
        // Game editing, if edit id was provided.
        $edit_id = $this->request->variable('edit', 0);
        $edit_game = null;
        if($edit_id > 0)
        {
            $edit_game = $this->lottery_manager->get_game($edit_id);

            if(!$edit_game)
            {
                trigger_error('MCP_LOTTERY_INVALID_GAME');
            }
        }

        $field_values = $this->get_field_values($edit_game);

		if($submit_posted)
		{
            /* TODO: Could transfer these Request->variable() calls into get_field_values() to clean up. Field
            values could include start_date and end_date. */
			$name = $this->request->variable('name', $field_values['name']);
			$ticket_cost = $this->request->variable('ticket_cost', $field_values['ticket_cost']);
			$max_tickets = $this->request->variable('max_tickets', $field_values['max_tickets']);
			$max_tickets_user = $this->request->variable('max_tickets_user', $field_values['max_tickets_user']);

			$sdary = array(
				'day' => $this->request->variable('start_date_day', $field_values['start_date_day']),
				'month' => $this->request->variable('start_date_month', $field_values['start_date_month']),
				'year' => $this->request->variable('start_date_year', $field_values['start_date_year']),
			);
			$edary = array(
				'day' => $this->request->variable('end_date_day', $field_values['end_date_day']),
				'month' => $this->request->variable('end_date_month', $field_values['end_date_month']),
				'year' => $this->request->variable('end_date_year', $field_values['end_date_year']),
			);

			$start_date = mktime(0, 0, 0, $sdary['month'], $sdary['day'], $sdary['year']);
			$end_date = mktime(0, 0, 0, $edary['month'], $edary['day'], $edary['year']);

			$errors = $this->validate_game_fields($name, $ticket_cost, $max_tickets, $max_tickets_user,
                $sdary, $edary, $start_date, $end_date);

			if(count($errors) == 0)
			{
                if($edit_id > 0)
                {
                    $this->lottery_manager->edit_game($edit_id, $name, $start_date, $end_date, 0, $ticket_cost,
                        $max_tickets, $max_tickets_user);
                }
                else
                {
				    $this->lottery_manager->create_game($name, $start_date, $end_date, 0, $ticket_cost, $max_tickets,
                        $max_tickets_user);
                }

				$play_url_params = array(
						'i=-forumpromotion-lottery-mcp-manage_lottery_module',
						'mode=create_game',
					);
				$return_url = append_sid($this->root_path . 'mcp.' . $this->phpEx, implode('&amp;', $play_url_params));

                $error_message = ($edit_id > 0) ?
                    $this->user->lang('MCP_LOTTERY_EDIT_SUCCEEDED') :
                    $this->user->lang('MCP_LOTTERY_CREATE_SUCCEEDED');

                trigger_error(
					$error_message
					. '<br /><br /><a href="' . $return_url . '">' 
					. $this->user->lang('MCP_LOTTERY_RETURN')
					. '</a>'
				);
			}
			else
			{
				$field_values['name'] = $name;

				$field_values['start_date_day']   = $sdary['day'];
				$field_values['start_date_month'] = $sdary['month'];
				$field_values['start_date_year']  = $sdary['year'];
				$field_values['end_date_day']     = $edary['day'];
				$field_values['end_date_month']   = $edary['month'];
				$field_values['end_date_year']    = $edary['year'];

				$field_values['ticket_cost'] = $ticket_cost;
				$field_values['max_tickets'] = $max_tickets;
				$field_values['max_tickets_user'] = $max_tickets_user;
			}
		}

		$this->set_game_form_template_vals($field_values, $errors, $edit_id);

		$this->tpl_name = 'mcp_create_game';
		$this->page_title = $this->user->lang('MCP_LOTTERY_CREATE_GAME');
	}

	public function create_recurring_game()
	{
		$submit_posted = $this->request->is_set_post('submit');

		$field_values = $this->get_field_values(null, true);
		$errors = array();

		if($submit_posted)
		{
            $recurring_enabled = $this->request->variable('recurring_enabled', false);
			$name = $this->request->variable('name', '');
			$game_length = $this->request->variable('game_length', 0);
			$ticket_cost = $this->request->variable('ticket_cost', 10);
			$max_tickets = $this->request->variable('max_tickets', -1);
			$max_tickets_user = $this->request->variable('max_tickets_user', -1);

			$errors = $this->validate_game_fields($name, $ticket_cost, $max_tickets, $max_tickets_user, NULL, NULL, NULL, NULL, TRUE, FALSE, $game_length);

			if(count($errors) == 0)
			{
				$changing_enable_values = ($this->request->variable('recurring_enabled', 0) != $this->config['lottery_recurring_enabled']);

				$this->config->set('lottery_recurring_enabled', $recurring_enabled);
				$this->config->set('lottery_recurring_name', $name);
				$this->config->set('lottery_recurring_ticket_cost', $ticket_cost);
				$this->config->set('lottery_recurring_length', $game_length);
				$this->config->set('lottery_recurring_max_tickets_user', $max_tickets_user);
				$this->config->set('lottery_recurring_max_tickets', $max_tickets);

				$update_affected_rows = $this->lottery_manager->update_recurring_game();

				if($update_affected_rows == 0 && $recurring_enabled)
				{
					$start_date = strtotime("midnight", time());
					$end_date = $start_date + ($game_length * 24 * 60 * 60);

					$this->lottery_manager->create_game($name, $start_date, $end_date, 0, $ticket_cost, $max_tickets, $max_tickets_user, TRUE);
				}

				$play_url_params = array(
					'i=-forumpromotion-lottery-mcp-manage_lottery_module',
					'mode=create_recurring_game',
				);
				$return_url = append_sid($this->root_path . 'mcp.' . $this->phpEx, implode('&amp;', $play_url_params));

				trigger_error(
					$this->user->lang('MCP_LOTTERY_CREATE_SUCCEEDED') 
					. '<br /><br /><a href="' . $return_url . '">' 
					. $this->user->lang('MCP_LOTTERY_RETURN')
					. '</a>'
				);
			}
			else
			{
				$field_values['name'] = $name;
				$field_values['length'] = $game_length;
				$field_values['ticket_cost'] = $ticket_cost;
				$field_values['max_tickets'] = $max_tickets;
				$field_values['max_tickets_user'] = $max_tickets_user;
			}
		}

		$this->set_game_form_template_vals($field_values, $errors);

		$this->template->assign_vars(array(
			'S_RECURRING' => TRUE,
			'S_RECURRING_ENABLED' => $this->config['lottery_recurring_enabled'],
		));

		$this->tpl_name = 'mcp_create_recurring_game';
		$this->page_title = $this->user->lang('MCP_LOTTERY_CREATE_RECURRING_GAME');
	}

	private function validate_game_fields($name, $ticket_cost, $max_tickets, $max_tickets_user, $sdary, $edary, $start_date, $end_date, $recurring = FALSE, $edit = FALSE, $game_length = 0)
    {
        $errors = array();

        $today_beginning = strtotime("midnight", time());
        $tomorrow_beginning = strtotime("tomorrow", $today_beginning) - 1;

        if($recurring)
        {
            if(!is_int($game_length) || $game_length <= 0)
            {
                $errors[] = $this->user->lang('MCP_LOTTERY_INVALID_GAME_LENGTH');
            }
        }
        else
		{
			// Date validation for start date.
			if(!checkdate($sdary['month'], $sdary['day'], $sdary['year']))
			{
				$errors[] = $this->user->lang('MCP_INVALID_START_DATE');
			}
			else
			{
				if($start_date < $today_beginning || !is_int($start_date))
				{
					$errors[] = $this->user->lang('MCP_LOTTERY_START_DATE_TOO_LOW');
				}
			}

			// Date validation for end date.
			if(!checkdate($edary['month'], $edary['day'], $edary['year']))
			{
				$errors[] = $this->user->lang('MCP_INVALID_END_DATE');
			}
			else
			{
				if($end_date < $tomorrow_beginning || !is_int($end_date) || $end_date <= $start_date)
				{
					$errors[] = $this->user->lang('MCP_LOTTERY_END_DATE_TOO_LOW');
				}
			}
		}

		if(strlen($name) > 50 || strlen($name) <= 2)
		{
			$errors[] = $this->user->lang('MCP_INVALID_NAME_LENGTH');
		}

		if(!is_int($ticket_cost) || $ticket_cost < 0)
		{
			$errors[] = $this->user->lang('MCP_LOTTERY_INVALID_TICKET_COST');
		}

		if(!is_int($max_tickets) || $max_tickets < 0)
		{
			$errors[] = $this->user->lang('MCP_LOTTERY_INVALID_MAX_TICKETS');
		}

		if(!is_int($max_tickets_user) || $max_tickets_user < 0)
		{
			$errors[] = $this->user->lang('MCP_LOTTERY_INVALID_MAX_TICKETS_USER');
		}

		return $errors;
	}

    /**
     * @param array $edit_game   Game that is being edited.
     * @param bool $recurring_game   Whether or not the game is recurring.
     * @return array   Contains field values given params.
     */
    private function get_field_values($edit_game = null, $recurring_game = false)
	{
		$end_date_default = time() + ($this->config['lottery_game_length'] * 60 * 60 * 24);
		$field_values = null;
        
		if($edit_game)
		{
			$field_values = array(
				'name' => $edit_game['game_name'],

				'start_date_day' => date("j", $edit_game['game_start_date']),
				'start_date_month' => date("n", $edit_game['game_start_date']),
				'start_date_year' => date("Y", $edit_game['game_start_date']),

				'end_date_day' => date("j", $edit_game['game_end_date']),
				'end_date_month' => date("n", $edit_game['game_end_date']),
				'end_date_year' => date("Y", $edit_game['game_end_date']),
                
                // Recurring only.
				'length' => (int) $this->config['lottery_recurring_name'],

				'ticket_cost' => (int) $edit_game['game_ticket_cost'],
				'max_tickets' => (int) $edit_game['game_max_tickets'],
				'max_tickets_user' => (int) $edit_game['game_max_tickets_user'],
			);
		}
		else
		{
			$field_values = array(
				'name' => ($recurring_game) ? $this->config['lottery_recurring_name'] : '',

				'start_date_day' => date("j"),
				'start_date_month' => date("n"),
				'start_date_year' => date("Y"),

				'end_date_day' => date("j", $end_date_default),
				'end_date_month' => date("n", $end_date_default),
				'end_date_year' => date("Y", $end_date_default),

				// Recurring only.
				'length' => (int) $this->config['lottery_recurring_length'],

				'ticket_cost' => (int) $this->config['lottery_ticket_cost'],
				'max_tickets' => (int) $this->config['lottery_ticket_round_max'],
				'max_tickets_user' => (int) $this->config['lottery_ticket_user_max'],
			);
		}

		return $field_values;
	}

	private function set_game_form_template_vals($field_values, $errors, $edit_id = false)
	{
		$ary_day_options = range(1, 31);
		$ary_month_options = range(1, 12);
		$this_year = (int) date("Y");
		$ary_year_options = range($this_year, $this_year + 3);

		$start_day_options = 
			$this->array_into_option_string($ary_day_options, $field_values['start_date_day']);
		$start_month_options = 
			$this->array_into_option_string($ary_month_options, $field_values['start_date_month']);
		$start_year_options = 
			$this->array_into_option_string($ary_year_options, $field_values['start_date_year']);
		$end_day_options = 
			$this->array_into_option_string($ary_day_options, $field_values['end_date_day']);
		$end_month_options = 
			$this->array_into_option_string($ary_month_options, $field_values['end_date_month']);
		$end_year_options = 
			$this->array_into_option_string($ary_year_options, $field_values['end_date_year']);

		$s_error = count($errors) > 0;
		$error_message = '';
		foreach($errors as $error_key => $error_val)
		{
			$error_message .= $error_val . ' ';
		}

		$this->template->assign_vars(array(
			'S_ERROR' => $s_error,
			'ERROR_MSG' => $error_message,
            'S_EDIT' => $edit_id,

			'S_LOTTERY_NAME' => $field_values['name'],
			'S_TICKET_COST' => $field_values['ticket_cost'],
			'S_GAME_LENGTH' => $field_values['length'],
			'S_LOTTERY_START_DAY_OPTIONS'   => $start_day_options,
			'S_LOTTERY_START_MONTH_OPTIONS' => $start_month_options,
			'S_LOTTERY_START_YEAR_OPTIONS'  => $start_year_options,
			'S_LOTTERY_END_DAY_OPTIONS'     => $end_day_options,
			'S_LOTTERY_END_MONTH_OPTIONS'   => $end_month_options,
			'S_LOTTERY_END_YEAR_OPTIONS'    => $end_year_options,
			'S_MAX_TICKETS' => $field_values['max_tickets'],
			'S_MAX_TICKETS_USER' => $field_values['max_tickets_user'],
		));
	}

	private function array_into_option_string($ary, $default_val = NULL)
	{
		$opt_tag_format = '<option%2$s value="%1$s">%1$s</option>';
		$selected_arg = 'selected="selected"';
		$ofTheJedi = '';
		$args = null;

		foreach($ary as $value)
		{
			$args = '';

			if($default_val == $value)
			{
				$args = ' ' . $selected_arg;
			}

			$ofTheJedi .= sprintf($opt_tag_format, (string) $value, $args);
		}

		return $ofTheJedi;
	}
}